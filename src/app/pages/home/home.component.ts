import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';

import {} from 'googlemaps';

@Component({
  selector: 'drun-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isOpen: boolean = false;

  inputCPF: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.inputCPF = this.formBuilder.group({
      cpf: [null, [Validators.required, GenericValidator.isValidCpf()]],
    });
  }

  eventSideMenu(event) {
    if (event) this.isOpen = event;
    else setTimeout(() => (this.isOpen = event), 300);
  }

  checkTest(): void {
    if (!this.inputCPF.invalid) {
      const CPF = this.inputCPF.controls.cpf.value;

      window.open(
        `http://consultas.detrannet.sc.gov.br/habilitacao/scripts/habilitacao.asp?cpf=${CPF}&submit=CONSULTAR+RESULTADO`
      );

      this.inputCPF.reset();
    }
  }

  goToPhone(): void {
    window.open('https://api.whatsapp.com/send?1=pt_BR&phone=5547997350464');
  }
  goToGoogleMaps(): void {
    window.open('https://www.google.com/maps?ll=-26.450983,-49.171593&z=15&t=m&hl=pt-BR&gl=BR&mapclient=embed&cid=6941015612982367315');
  }
}

export class GenericValidator {
  constructor() {}

  static isValidCpf() {
    return (control: AbstractControl): Validators => {
      const cpf = control.value;
      if (cpf) {
        let numbers, digits, sum, i, result, equalDigits;
        equalDigits = 1;
        if (cpf.length < 11) {
          return { cpfNotValid: true };
        }

        for (i = 0; i < cpf.length - 1; i++) {
          if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
            equalDigits = 0;
            break;
          }
        }

        if (!equalDigits) {
          numbers = cpf.substring(0, 9);
          digits = cpf.substring(9);
          sum = 0;
          for (i = 10; i > 1; i--) {
            sum += numbers.charAt(10 - i) * i;
          }

          result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

          if (result !== Number(digits.charAt(0))) {
            return { cpfNotValid: true };
          }
          numbers = cpf.substring(0, 10);
          sum = 0;

          for (i = 11; i > 1; i--) {
            sum += numbers.charAt(11 - i) * i;
          }
          result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

          if (result !== Number(digits.charAt(1))) {
            return { cpfNotValid: true };
          }
          return null;
        } else {
          return { cpfNotValid: true };
        }
      }
      return null;
    };
  }
}
