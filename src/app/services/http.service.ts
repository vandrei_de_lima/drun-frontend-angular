import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root',
})
export class HttpService {
  url = 'https://auto-escola-drun.herokuapp.com';
}
