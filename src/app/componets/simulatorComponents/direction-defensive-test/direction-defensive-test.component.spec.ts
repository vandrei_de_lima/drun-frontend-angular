import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectionDefensiveTestComponent } from './direction-defensive-test.component';

describe('DirectionDefensiveTestComponent', () => {
  let component: DirectionDefensiveTestComponent;
  let fixture: ComponentFixture<DirectionDefensiveTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectionDefensiveTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectionDefensiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
