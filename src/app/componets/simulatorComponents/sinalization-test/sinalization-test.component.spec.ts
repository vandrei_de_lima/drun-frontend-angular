import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinalizationTestComponent } from './sinalization-test.component';

describe('SinalizationTestComponent', () => {
  let component: SinalizationTestComponent;
  let fixture: ComponentFixture<SinalizationTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinalizationTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinalizationTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
