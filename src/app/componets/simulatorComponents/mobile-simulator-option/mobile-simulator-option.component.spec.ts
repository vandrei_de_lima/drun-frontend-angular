import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileSimulatorOptionComponent } from './mobile-simulator-option.component';

describe('MobileSimulatorOptionComponent', () => {
  let component: MobileSimulatorOptionComponent;
  let fixture: ComponentFixture<MobileSimulatorOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileSimulatorOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileSimulatorOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
