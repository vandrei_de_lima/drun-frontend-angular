// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firbase: {
    apiKey: 'AIzaSyD74WBULxUQ7qirS8aCxZXHEC1JYiJ-8J0',
    authDomain: 'auto-escola-drun-front.firebaseapp.com',
    projectId: 'auto-escola-drun-front',
    storageBucket: 'auto-escola-drun-front.appspot.com',
    messagingSenderId: '894618088694',
    appId: '1:894618088694:web:ce8ae090e86284c8b6e8d7',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
